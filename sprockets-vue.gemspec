$:.unshift File.expand_path("../lib", __FILE__)
require 'sprockets/vue/version'

Gem::Specification.new do |s|
  s.name    = 'sprockets-vue'
  s.version = Sprockets::Vue::VERSION

  s.homepage    = ""
  s.summary     = ""
  s.description = <<-EOS
                Sprockets transformer for .vue files.
  EOS
  s.license = "MIT"

  s.files = Dir["README.md", "MIT-LICENSE", "lib/**/*.rb"]

  s.add_dependency 'sprockets', '>= 3.0.0'
  s.add_dependency 'sprockets-es6'
  s.add_dependency 'actionview'
  s.add_dependency 'activesupport'
  s.add_development_dependency 'execjs'
  s.add_development_dependency 'sass'
  s.add_development_dependency 'coffee-script'
  s.add_development_dependency 'slim'

  s.authors = ['domenik']
  s.email   = 'werderda@yahoo.com'
end
