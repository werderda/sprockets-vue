# sprockets-vue

A [Sprockets](https://github.com/rails/sprockets) transformer that converts .vue file into js object.

# feature

following tags are supported in .vue file
* script (coffeescript, es6 and js)
* template (slim and html)
* style (scss, sass and css)

# install
add `gem 'sprockets-vue', git: 'git@bitbucket.org/werderda/sprockets-vue.git', branch: 'master'` to Gemfile, and run bundle
add `config.assets.paths << Rails.root.join("components")` to config/application.rb

# example
* app/assets/components/test/index.vue
```vue
<script lang="es6">
  module.exports = {
    props: ['m'],
    data: () => {
      return {
        member: {},
        expand: false
      };
    },
    computed: {
      avatar_url() {
        if (this.m.avatar) {
          return this.m.avatar + '?imageView2/2/w/60/h/60/q/100';
        } else {
          return 'http://ww2.sinaimg.cn/large/801b780ajw1f8zvq84qkkj2040040wek.jpg';
        }
      }
    }
  }
</script>

<template>
  <div class="panel panel-default" @click='expand=!expand'>
    <div class="panel-heading">
      <h3 class="panel-title">{{m.name}}
        <span class="fa-icon-user"></span>
      </h3>
    </div>
    <div class="panel-body">
      <ul class="media-list">
        <li class="media">
          <div class="media-left">
            <img class="avatar media-object" :src="avatar_url">
          </div>
          <div class="media-body">
            <div class="media-heading"><b>{{m.position}}</b></div>
            <div> {{m.company}}</div>
            <div>
              <a :href="m.phone">{{m.phone}}</a>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</template>

<style lang="scss">
  .vue-test {
    .avatar {
      width: 60px;
      height: 60px;
    }
  }
</style>
```

* app/assets/components/test/card.vue
```vue
<script lang="es6">
  module.exports = {
    props: ['m'],
    data: () => {
      return {
        member: {},
        expand: false
      };
    },
    computed: {
      avatar_url() {
        if (this.m.avatar) {
          return this.m.avatar + '?imageView2/2/w/60/h/60/q/100';
        } else {
          return 'http://ww2.sinaimg.cn/large/801b780ajw1f8zvq84qkkj2040040wek.jpg';
        }
      }
    }
  }
</script>

<template>
  <div class="panel panel-default" @click='expand=!expand'>
    <div class="panel-heading">
      <h3 class="panel-title">{{m.name}}
        <span class="fa-icon-user"></span>
      </h3>
    </div>
    <div class="panel-body">
      <ul class="media-list">
        <li class="media">
          <div class="media-left">
            <img class="avatar media-object" :src="avatar_url">
          </div>
          <div class="media-body">
            <div class="media-heading"><b>{{m.position}}</b></div>
            <div> {{m.company}}</div>
            <div>
              <a :href="m.phone">{{m.phone}}</a>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</template>

<style lang="scss">
  .vue-test {
    .avatar {
      width: 60px;
      height: 60px;
    }
  }
</style>
```

* app/assets/stylesheets/components.css
```css
/*
 *= require test/index
*/
```

* app/assets/javascripts/components.js
```js
//= require test/index
```

* somewhere
```slim
#test
  test-index
javascript:
  $(function() {
    new Vue({
      el: '#test',
      components: {
        'test-index': vComponents['test/index']
      }
    });
  });
```
