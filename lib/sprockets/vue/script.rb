require 'active_support/concern'
require "action_view"
module Sprockets::Vue
  class Script
    class << self
      include ActionView::Helpers::JavaScriptHelper

      SCRIPT_REGEX = Utils.node_regex('script')
      TEMPLATE_REGEX = Utils.node_regex('template')
      SCRIPT_COMPILES = {
        'coffee' => ->(s, input) {
          CoffeeScript.compile(s, sourceMap: true, sourceFiles: [input[:source_path]], no_wrap: true)
        },
        'es6' => ->(s, input) {
          Babel::Transpiler.transform(s, {
            'sourceRoot' => input[:load_path],
            'moduleRoot' => nil,
            'filename' => input[:filename],
            'filenameRelative' => input[:environment].split_subpath(input[:load_path], input[:filename])
          });
        },
        nil => ->(s, input){ { 'js' => s } }
      }
      TEMPLATE_COMPILES = {
        'slim' => ->(s, input) {
          Slim::Template.new(input[:name]) { s }.render(nil)
        },
        nil => ->(s, input){ { 'html' => s } }
      }
      def call(input)
        data = input[:data]
        name = input[:name]

        input[:cache].fetch([cache_key, input[:source_path], data]) do
          script = SCRIPT_REGEX.match(data)
          template = TEMPLATE_REGEX.match(data)
          output = []
          map = nil
          if script
            result = SCRIPT_COMPILES[script[:lang]].call(script[:content], input)

            map = result['sourceMap']

            case script[:lang]
            when 'es6'
              output << "
'object' != typeof vComponents && (this.vComponents = {});
var module = { exports: null };
#{result['code']}
vComponents['#{name}'] = module.exports;
"
            else
              output << "
'object' != typeof vComponents && (this.vComponents = {});
var module = { exports: null };
#{result['js']};
vComponents['#{name}'] = module.exports;
"
            end
          end

          if template
            case template[:lang]
            when 'slim'
              result = TEMPLATE_COMPILES[template[:lang]].call(template[:content], input)
              output << "vComponents['#{name.sub(/\.tpl$/, "")}'].template = '#{j result}';"
            else
              output << "vComponents['#{name.sub(/\.tpl$/, "")}'].template = '#{j template[:content]}';"
            end
          end

          { data: "#{warp(output.join)}", map: map }
        end
      end

      def warp(s)
        "(function(){#{s}}).call(this);"
      end

      def cache_key
        [
          self.name,
          VERSION,
        ].freeze
      end
    end
  end
end
